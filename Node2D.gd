extends Node

const CHUNK_SIZE = 10
const CELL_SIZE = 16

export(NodePath) var objectsSpace = null
var start_pos : Vector2
var end_pos   : Vector2
var navPolygon = null

var simulatedChunks = [Vector2(0,0),Vector2(0,1), Vector2(0,2),Vector2(1,0),Vector2(1,1),Vector2(2,2),Vector2(1,2),Vector2(2,1),Vector2(2,0)]

func _ready():
	navPolygon = create_navigation()
	create_polygons(get_node("Node2D").get_children(), navPolygon)
	$Navigation2D/NavigationPolygonInstance.navpoly = navPolygon

func create_navigation():
	var nav = NavigationPolygon.new()
	var minX = null
	var maxX = null
	var minY = null
	var maxY = null
	for chunk in simulatedChunks:
		if minX == null or maxX == null or minY == null or maxY == null: 
			minX = chunk.x
			maxX = chunk.x
			minY = chunk.y
			maxY = chunk.y
		minX = min(minX, chunk.x)
		maxX = max(maxX, chunk.x)
		minY = min(minY, chunk.y)
		maxY = max(maxY, chunk.y)
	minX*=CHUNK_SIZE*CELL_SIZE
	minY*=CHUNK_SIZE*CELL_SIZE
	maxX*=CHUNK_SIZE*CELL_SIZE
	maxY*=CHUNK_SIZE*CELL_SIZE
	
	var arr:PoolVector2Array = [Vector2(minX,minY),Vector2(maxX,minY),Vector2(maxX,maxY),Vector2(minX,maxY)]
	nav.add_outline(arr)
	nav.make_polygons_from_outlines()
	return nav

func create_polygons(objArray, nav):
	for obj in objArray:
		var raw = obj.navPoly.get_outline(0)
		var poly:PoolVector2Array = []
		for p in raw:
			var point = p+obj.global_position
			poly.append(point)
		nav.add_outline(poly)
		nav.make_polygons_from_outlines()

func _unhandled_input(event):
	if event is InputEventMouseButton and event.is_pressed():
		print("clicked")
		if   event.button_index   == BUTTON_LEFT:
			start_pos = event.global_position
			print(start_pos)
		elif event.button_index == BUTTON_RIGHT:
			end_pos   = event.global_position
		if start_pos and end_pos:
			redraw()

func redraw():
	$Line2D.set_points($Navigation2D.get_simple_path(start_pos, end_pos))