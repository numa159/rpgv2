shader_type canvas_item;

uniform float grid_mult;

vec2 N22(vec2 pos)
{
	vec3 a = fract(vec3(pos.xy, pos.x) * vec3(123.34, 264.2, 345.36));
	a+=dot(a, a+45.58);
	return fract(vec2(a.x*a.y,a.y*a.z));
}

float getVal(vec3 col) {
	return (col.x + col.y + col.z)/3.;
}

float getVal(vec4 col) {
	return (col.x + col.y + col.z)/3.;
}

void fragment()
{
	vec2 sRes = (1.0/SCREEN_PIXEL_SIZE);
	vec2 uv = (UV)*sRes.x/sRes.y;
	float m = .0;
	float t = TIME*.5;
	
	float minDist = 100.;
//	if (false) {
//		for(float i = 0.; i<(points); i++) {
//			vec2 n = N22(vec2(i,i));
//			vec2 p = sin(n*t);
//			
//			float dist = length(uv-p);
//			m += smoothstep(.05,.03,dist);
//			minDist = min(minDist, length(uv-p));
//		}
//	} else {
	uv *= grid_mult;
	vec2 gv = fract(uv)-.5;
	vec2 id = floor(uv);
	
	vec2 cid = vec2(0.);
	
	for(float x = -1.; x<=1.; x++) {
		for(float y = -1.; y<=1.; y++) {
			vec2 offs = vec2(x,y);
		
			vec2 n = N22(id+offs);
			vec2 p = offs + sin(n*t)*.5;
			
			float dist = length(gv-p);
			minDist = min(dist, minDist);
			cid = mix(cid, id+offs, step(dist,minDist));
		}
	}
	
	
//	}
//	float val = smoothstep(.5,.0,minDist);
//	vec3 col2 = vec3(((cos(TIME*2.)+1.)/2.),((sin(TIME*4.)+1.)/2.),((sin(TIME*2.)+1.)/2.));
//	vec3 col1 = vec3(((sin(TIME*2.)+1.)/2.),((cos(TIME*4.)+1.)/2.),((cos(TIME*2.)+1.)/2.));
//	vec3 col = vec3(mix(col1, col2, val));
//	vec3 col = vec3(cid*.1,0.);
	vec3 col = vec3(sin(N22(cid).x+t)*.5+.5,sin(N22(cid).y+t)*.5+.5,cos(N22(cid).x*.5+N22(cid).y*2.+t)*.5+.5);
	float val = getVal(col);
	float dh = .3;
	float uh = .7;
	float k = step(dh, val)-.2;
	//float q = step(uh, val);
	COLOR.rgb = col;
//	COLOR.rg = uv;
}