shader_type canvas_item;

uniform float grid_mult:hint_range(.1,1000.0);
uniform float radius : hint_range(0.0,1.0);
uniform float cutoff : hint_range(0.0,0.2);


vec2 centerUV(vec2 uv, vec2 screenRes)
{
	vec2 n_uv = uv;
	n_uv -= .5;
	n_uv.x*=screenRes.x/screenRes.y;
	return n_uv;
}

float circle(float r, float blur, vec2 uv, vec2 pos)
{
	return smoothstep(r, r-blur, length(uv-pos));
}

vec2 gridify(vec2 uv, float gridMult)
{
	return fract(uv*gridMult)-.5;
}

void fragment()
{
	vec2 sRes = vec2(800,600);
	vec2 uv = centerUV(UV, sRes);
	vec2 gv = gridify(uv, grid_mult);
	vec2 gid = floor(uv*grid_mult);
	gid = abs(gid);
	float rad = radius*abs(sin(TIME+length(uv)));
	float val = circle(rad, cutoff, gv, vec2(.0));
	val -= circle(rad/2.0, cutoff, gv, vec2(cos(TIME*(length(gid)+1.0))*.25,sin(TIME*(length(gid)+1.0))*.25));
//	COLOR.rgb = vec3(val);
	COLOR.rgb = abs(vec3(sin(TIME*2.0+length(uv)),sin(TIME*1.5+length(uv)),sin(TIME+length(uv)))*val);
}
