extends Node2D

export(PackedScene) var enemieScene = preload("res://Scenes/Entities/OldHumanoid.tscn")
var spawning:=false
var enemies := []

func _on_SpawnEnemie_toggled(button_pressed):
	spawning = button_pressed

func _unhandled_input(event):
	if event is InputEventMouseButton and event.is_pressed() and event.button_index == BUTTON_LEFT and spawning:
		enemies.append(spawn_enemie(\
		($Character/Cam as Camera2D).get_camera_screen_center() -\
		(Vector2(ProjectSettings.get("display/window/size/width"),\
		ProjectSettings.get("display/window/size/height"))/2.0) +\
		event.position))

func spawn_enemie(pos)->PackedScene:
	var enemie = enemieScene.instance()
	enemie.global_position = pos
	add_child(enemie)
	return enemie