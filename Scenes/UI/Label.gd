extends Label

func _process(delta):
	set_text(str("FPS: ", Performance.get_monitor(Performance.TIME_FPS)))