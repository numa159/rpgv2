extends VBoxContainer

func _on_DONE_pressed():
	var nfactions = Entities.factions
	var facname = $Name.text
	for fac in nfactions.keys():
		if !$Relations.fromRelations.has(fac) or !$Relations.toRelations.has(fac) or facname == "FactionName":
			return
		nfactions[fac].relations[facname] = $Relations.fromRelations[fac]
	nfactions[facname] = {"relations" : $Relations.toRelations, "default" : $Relations.defaultRel}
	Entities.factions = nfactions
	get_tree().reload_current_scene()