extends MenuButton

var popup = null

signal relation_chosen

func _ready():
	popup = get_popup()
	for relation in Enum.Relation.keys():
		popup.add_item(relation)
	popup.connect("id_pressed", self, "_on_item_pressed")

func _on_item_pressed(ID):
	emit_signal("relation_chosen", popup.get_item_text(ID), self.name)