extends KinematicBody2D

const CHARG_MULT = .5
const CHAR_PATH = "res://Assets/Sprites/char.png"

export(PackedScene) var weaponScene = null
export(PackedScene) var bulletScene = null

export(float) var roll_duration = 1
export(float) var roll_cd = 2
export(float) var speed = 0
export(float,1.0,100.0) var attackSpd = 10
export(String) var factionName = "Humans"

var faction = {}
var charging := false
var isPlayer := true
var canMove := true
var rolling := false
var attacking := false
var roll_cooldown := false
var direction := Vector2(0,0)
export(float) var health := 100
var hand = null
var weapon = null
var chargeTime := .1

signal rolled

func _init():
	connect("rolled", self, "roll_cool")

func recieve_damage(damage, emitter):
	health -= damage
	if health <= 0:
		print("DEAD")

func _ready():
#	set_process(false)
	faction = Entities.factions[factionName]
	CharacterData.ref = self
	var sprite = load(CHAR_PATH)
	$Sprite.texture = sprite
	$Sprite.texture.set_flags(0)
	if weaponScene:
		var wp = weaponScene.instance()
		weapon = wp
		$Weapons.add_child(weapon)
		weapon.heldBy = self

func _unhandled_input(event):
	if canMove and !rolling:
		direction = get_direction()
	if event.is_action("roll") and event.is_pressed() and direction != Vector2(0,0) and not rolling and !roll_cooldown and canMove and not charging:
		roll()
	elif event is InputEventMouseButton and event.button_index == BUTTON_LEFT and canMove and !rolling:
		var pressed = event.is_pressed()
		if pressed:
			start_charging()
		else:
			var dir = ($Cam.get_camera_screen_center()-Vector2(ProjectSettings.get("display/window/size/width")/2.0,ProjectSettings.get("display/window/size/height")/2.0) + event.position - self.global_position).normalized() 
			fire(dir)
	elif event is InputEventMouseButton and event.button_index == BUTTON_RIGHT and canMove and !rolling and event.is_pressed() and !attacking and !charging and weapon:
		var dir = ($Cam.get_camera_screen_center()-Vector2(ProjectSettings.get("display/window/size/width")/2.0,ProjectSettings.get("display/window/size/height")/2.0) + event.position - self.global_position).normalized() 
		attack(dir)

func get_direction()->Vector2:
	var dir = direction
	if Input.is_action_pressed("move_down") and !Input.is_action_pressed("move_up"):
		dir.y=1
	elif Input.is_action_pressed("move_up") and !Input.is_action_pressed("move_down"):
		dir.y=-1
	else:
		dir.y = 0
	if Input.is_action_pressed("move_left") and !Input.is_action_pressed("move_right"):
		dir.x=-1
	elif Input.is_action_pressed("move_right") and !Input.is_action_pressed("move_left"):
		dir.x=1
	else:
		dir.x = 0
	return dir

func attack(dir:Vector2):
	weapon.attack(dir, attackSpd)

func start_charging():
	charging = true
	if !hand:
		hand = bulletScene.instance()
		hand.emitter = self
		$Weapons.add_child(hand)
	hand.global_position = self.global_position

func fire(direction:Vector2):
	charging = false
	if !hand:
		return
	hand.direction = direction
	hand.charge = chargeTime
	hand.get_parent().remove_child(hand)
	get_parent().get_node("Proyectiles").add_child(hand)
	hand.global_position = self.global_position
	hand.emit_signal("fired")
	hand = null
	chargeTime = .1

func roll():
	rolling = true
	if direction.x > 0 :
		$Anim.play("roll_right")
	else:
		$Anim.play("roll_left")
	yield(get_tree().create_timer(roll_duration), "timeout")
	rolling = false
	roll_cooldown = true
	emit_signal("rolled")

func roll_cool():
	yield(get_tree().create_timer(roll_cd), "timeout")
	roll_cooldown = false

func _process(delta):
	CharacterData.pos = self.position
	if direction != Vector2(0,0) and not rolling:
		$Anim.play("walk")
	elif not rolling:
		$Anim.play("idle")
	if hand:
		chargeTime+=.1
		hand.rotation = get_local_mouse_position().normalized().angle()
	if canMove and direction != Vector2(0,0):
		var spd = speed
		if charging:
			spd*=CHARG_MULT
		self.move_and_slide(direction.normalized() * spd)

func on_enemie_killed(enemie):
	pass