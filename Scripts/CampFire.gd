extends SpawnerObject

enum States {NULL=-1, OFF, ON, SMOKING}

export(int) var state = States.NULL setget set_state

func _ready():
	if state == States.NULL:
		set_state(randi() % States.size())

func set_state(n_st:int):
	if !is_inside_tree():
		return
	state = n_st
	match state:
		States.OFF:
			$SmokeEffect.set_emitting(false)
			$FireEffect.set_emitting(false)
			$Light.set_enabled(false)
		States.ON:
			$SmokeEffect.set_emitting(true)
			$FireEffect.set_emitting(true)
			$Light.set_enabled(true)
		States.SMOKING:
			$SmokeEffect.set_emitting(true)
			$FireEffect.set_emitting(false)
			$Light.set_enabled(false)