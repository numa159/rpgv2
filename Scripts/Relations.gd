extends Control

var fromRelations := {}
var toRelations := {}
var defaultRel := 0

const facField = preload("res://Tools/FactionField.tscn")

func _ready():
	for fac in Entities.factions.keys():
		var field = facField.instance()
		field.name = fac
		$VBox.add_child(field)
		fromRelations[fac] = defaultRel
		toRelations[fac] = defaultRel