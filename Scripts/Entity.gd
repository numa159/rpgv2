extends KinematicBody2D
class_name Entity

const SPRITES_FOLDER = "res://Assets/Sprites/"

const MAX_F = 20
const DIR_CD = 150
const TARGET_CD = 200

export(float)         var attackSpd = 10
export(float)         var health = 10
export(float)         var moveSpeed = 50
export(float)         var hostileRange = 150
export(float)         var maxForce = 20
export(bool)          var isHostile  = false setget set_hostility, get_hostility
export(PackedScene)   var weaponScene = null
export(String)        var spriteName
export(String)        var factionName
export(String)        var ref

var spriteIdx := -1
var faction := {} 
var nextTarget := 0
var attacking := false
var weapon = null
var isFollowing := false
var hostileTowards = null
var hostileList := []
var calculatorThread := Thread.new()
var attackType :int = -1
var isPlayer   := false
var direction  := Vector2.ZERO
var canMove    := true setget set_moving, get_moving
var nextDirTime := 0
var currentVel := Vector2.ZERO
var desiredVel := Vector2.ZERO
var accel := Vector2.ZERO

var _bodiesDetected := []
var _enemies := []

signal on_death

func _ready()->void:
	_set_sprite()
	_initialize_as_entity()
	_exclude_colliders()
	_set_surrounding_area()

func _set_sprite():
	var sprites := []
	var dir := Directory.new()
	var foldersProcessed := []
	
	if spriteName == "":
		print("Error: no spriteName given to entity.")
		assert(false)
	if dir.change_dir(str(SPRITES_FOLDER,spriteName)) != OK:
		print("Error: sprite not found.")
		assert(false)
	
	#Process files
	dir.list_dir_begin(true, true)
	var file : String = dir.get_next()
	var subfolder := ""
	while true:
		if foldersProcessed.has(file) or file.ends_with(".import"):
			file = dir.get_next()
		elif file == "":
			dir.change_dir("..")
			dir.list_dir_begin(true, true)
			subfolder = ""
			file = dir.get_next()
		elif dir.current_is_dir():
			foldersProcessed.append(file)
			dir.list_dir_end()
			dir.change_dir(file)
			dir.list_dir_begin(true, true)
			subfolder = file
			file = dir.get_next()
		else:
			if subfolder != "":
				sprites.append(str(subfolder, "/", file))
			else:
				sprites.append(file)
			file = dir.get_next()
		if (file == "" and subfolder == ""):break
	if spriteIdx < 0:
		spriteIdx = randi() % sprites.size()
	var sprite = sprites[spriteIdx]
	var tex = load(str(SPRITES_FOLDER,"/",spriteName,"/",sprite))
	$Sprite.set_texture(tex)

func _set_surrounding_area():
	$Surrounds/Shape.shape.set("radius", hostileRange)

func _exclude_colliders():
	$FwdRay.add_exception(CharacterData.ref)
	$FwdRay/Left.add_exception(CharacterData.ref)
	$FwdRay/Right.add_exception(CharacterData.ref)

func _initialize_as_entity()->void:
	if !weaponScene:
		print("Entity has no weapon scene")
	else:
		weapon = weaponScene.instance()
		add_child(weapon)
		weapon.heldBy = self
		attackType = weapon.attackType
	if factionName: faction = Entities.factions[factionName]
	else: print("Entity has no faction")

func _process(delta)->void:
	var time = OS.get_system_time_msecs()
	if time > nextTarget or !hostileTowards:_choose_enemie()
	if !calculatorThread.is_active() and time > nextDirTime:calculatorThread.start(self, "_get_desiredVel", null)
	if isFollowing and !attacking and hostileList.size():
		if attackType:
			if (self.position - hostileTowards.position).length() < weapon.wpnRange:
#				print("Hostiles: ", hostileList.size())
				weapon.attack(currentVel.normalized(), attackSpd)
		else:
			print("ranged atk")
	if currentVel != Vector2.ZERO and currentVel.length() > 10:
		$Anim.play("walk")
		move_and_slide(currentVel)
	else:
		$Anim.play("idle")

func _get_desiredVel(nullVoid):
	var dir = Vector2.ZERO
	if isHostile:
		dir = _get_direction_to_hostile()
	if dir == Vector2.ZERO:
		isFollowing = false
		dir = _get_wander_direction()
	else:
		isFollowing = true
	dir = _avoid_obstacles(dir)
	call_deferred("_finish_velocity", dir)

func _avoid_obstacles(dir):
	var angle = dir.angle()
	$FwdRay.rotation = angle
	if $FwdRay.is_colliding() and !($FwdRay.get_collider() is KinematicBody2D):
		if $FwdRay/Left.is_colliding() and !($FwdRay/Left.get_collider() is KinematicBody2D):
			dir = $FwdRay/Right.cast_to.rotated(angle).normalized()
		elif $FwdRay/Right.is_colliding() and !($FwdRay/Right.get_collider() is KinematicBody2D):
			dir *= -1
		else:
			dir = $FwdRay/Left.cast_to.rotated(angle).normalized()
	return dir

func _finish_velocity(dir:Vector2):
	if calculatorThread.is_active():calculatorThread.wait_to_finish()
	var spd = moveSpeed
	if !isFollowing:
		spd *= randf()*.5
	desiredVel = dir.normalized()*spd
	accel = desiredVel - currentVel
	accel = accel.normalized() * min(maxForce, accel.length())
	currentVel += accel
	nextDirTime = OS.get_system_time_msecs() + DIR_CD

func _get_direction_to_hostile() -> Vector2:
	var vec = hostileTowards.global_position - global_position
	if vec.length_squared() >= hostileRange*hostileRange:
		return Vector2.ZERO
	return vec.normalized()

func _get_wander_direction()-> Vector2:
	return currentVel.normalized()+Vector2.ONE.normalized().rotated(randf()*2*PI)*.2

func _sort_by_distance_from_self(node_a, node_b)->bool:
	if node_a != null and node_b != null: return (node_a.position-self.position) < (node_b.position-self.position)
	else: return true

func _choose_enemie():
	return

func recieve_damage(damage:float, emitter)->float:
	if emitter:
		hostileTowards = emitter
		set_hostility(true)
		if !hostileList.has(emitter):
			hostileList.append(emitter)
			connect("on_death", emitter, "on_enemie_killed")
		nextTarget = OS.get_system_time_msecs() + TARGET_CD
	health-=damage
	$Blood.emitting = false
	$Blood.emitting = true
	if health <= 0:
		$Sprite.hide()
		$CollisionShape2D.set_deferred("disabled", true)
		emit_signal("on_death", self)
		if !emitter.isPlayer: emitter.on_enemie_killed(self)
		yield(get_tree().create_timer($Blood.lifetime/$Blood.speed_scale), "timeout")
		die()
	yield(get_tree().create_timer($Blood.lifetime/$Blood.speed_scale), "timeout")
	return health

func die()->void:
	self.hide()
	if calculatorThread.is_active():calculatorThread.wait_to_finish()
	queue_free()

func set_moving(val:bool)->void:
	if direction == Vector2.ZERO:
		return
	canMove = val
	set_process(canMove)

func get_moving()->bool:
	return canMove

func set_hostility(val)->void:
	isHostile = val
#	if isHostile:
#		if faction == Enum.Factions.NEUTRAL:
#			faction = Enum.Factions.HOSTILE
#		else:
#			faction = Enum.Factions.NEUTRAL

func get_hostility()->bool:
	return isHostile

func on_enemie_killed(enemie):
	if hostileList.has(enemie):
		hostileList.erase(enemie)
		if hostileTowards == enemie:
			_choose_enemie()

func _on_Surrounds_body_entered(body):
	if body is KinematicBody2D and body != self:
		_bodiesDetected.append(body)

func _on_Surrounds_body_exited(body):
	if body is KinematicBody2D and _bodiesDetected.has(body):
		_bodiesDetected.erase(body)
