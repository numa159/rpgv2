extends CenterContainer

func _on_EC_pressed():
	get_tree().change_scene("res://Tools/EntityCreator.tscn")

func _on_SC_pressed():
	get_tree().change_scene("res://Tools/SpriteCreator.tscn")

func _on_OC_pressed():
	get_tree().change_scene("res://Tools/ObjectCreator.tscn")

func _on_CC_pressed():
	get_tree().change_scene("res://Tools/CharacterCreator.tscn")

func _on_Back_pressed():
	get_tree().change_scene("res://Scenes/Main.tscn")
