extends Bullet

const FREQ = 10
const AMPL = 1

onready var phase_shift:float=rand_range(0, PI*2)

func fly(dir, delta):
	position += Vector2(speed, sin(delta*FREQ+phase_shift)*AMPL/(charge)).rotated(dir.angle())

func collide():
	fired = false
	$Light.set_enabled(false)
	$Effect.set_emitting(false)
	$Smoke.set_emitting(false)
	$CollisionEffect.set_emitting(true)
	yield(get_tree().create_timer(max($CollisionEffect.lifetime/$CollisionEffect.speed_scale, $Smoke.lifetime/$Smoke.speed_scale)), "timeout")
	self.call_deferred("free")