extends KinematicBody2D

const FOLLOW_DIST = 300
const VIEW_DIST = 200
const SPEED = 50

export(float) var hit_range = 5
export(float) var knockStrength = 1000
export(int) var damage = 2
export(float) var health = 20
export(float) var hitCd = .4

var isPlayer := false
var dir:Vector2
var lookPos:Vector2 = Vector2(0,0)
var last_pos:Vector2
var moving : bool = false
var following : bool = false
var canMove : bool = true
var hitting : bool = false
var hitCdTimer = Timer.new()

func recieve_damage(damage):
	health-=damage
	if health<= 0:
		if hitCdTimer.time_left != 0:
			self.hide()
			yield(hitCdTimer, "timeout")
		queue_free()

func _on_cooldown_end():
	hitCdTimer.stop()
	hitting = false

func _ready():
	set_physics_process(false)
	add_child(hitCdTimer)
	hitCdTimer.connect("timeout", self, "_on_cooldown_end")
	hitCdTimer.set_wait_time(hitCd)
	last_pos = position
	for ray in $Rays.get_children():
		ray.set_collide_with_areas(false)
		ray.add_exception(self)
		ray.add_exception(CharacterData.ref)
	$ViewRay.set_collide_with_areas(false)
	$ViewRay.add_exception(self)
	$ViewRay.add_exception(CharacterData.ref)

func _process(delta):
	lookPos = CharacterData.pos
	dir = $Rays/FrontRay.cast_to.rotated($Rays.rotation).normalized()
	$Rays.rotation = get_angle_to(lookPos)
	$ViewRay.cast_to = lookPos-self.global_position

func _physics_process(delta):
	if !$ViewRay.is_colliding():
		following = true
	if following and canMove:
		walk_towards_player()

func walk_towards_player()->void:
	var dist : float   = position.distance_to(lookPos)
	for ray in $Rays.get_children():
		if !ray.is_colliding():
			dir = ray.cast_to.rotated($Rays.rotation).normalized()
			break
	dir = dir.normalized() * SPEED
	move_and_slide(dir)
	if position != last_pos:
		moving = true
		$Anim.play("walk")
	else:
		moving = false
		$Anim.play("idle")

func hit(entity, dir):
	if hitting:
		return
	hitting = true
	entity.recieve_damage(damage)
	knockback(entity, dir)
	hitCdTimer.start()

func knockback(entity, dir):
	var force = dir.normalized()*knockStrength
	entity.canMove = false
	var t = Tween.new()
	add_child(t)
	t.set_active(true)
	t.interpolate_property(entity, "position", entity.get_position(), entity.get_position() + force, .05, Tween.TRANS_QUAD, Tween.EASE_IN)
	t.start()
	yield(t, "tween_completed")
	t.queue_free()
	entity.canMove = true

func _on_ViewArea_body_entered(body):
	if body is KinematicBody2D and body.isPlayer:
		set_physics_process(true)

func _on_FollowArea_body_exited(body):
	if body is KinematicBody2D and body.isPlayer:
		set_physics_process(false)
		following = false
		moving = false
		$Anim.play("idle")

func _on_HitRange_body_entered(body):
	if body is KinematicBody2D and body.isPlayer and hitCdTimer.is_stopped():
		hit(CharacterData.ref, dir)