extends VBoxContainer

const PIX = preload("res://Tools/Pixel.tscn")
const COL = preload("res://Tools/ColorGroup.tscn")

var color = Color.white
var width : int = 1 setget set_width
var height : int = 1 setget set_height
var cleaning := false

signal done
signal done_cleaning

func _ready():
	var pixel = PIX.instance()
	$Sprite.add_child(pixel)

func _resize():
	for p in $Sprite.get_children():
		p.queue_free()
	$Sprite.columns = width
	$SeparateColors/PixelColors.columns = width
	for i in width*height:
		create_pixel()

func create_pixel():
		var pixel = PIX.instance()
		$Sprite.add_child(pixel)
		pixel.connect("pixel_set", self, "_on_pixel_set")
		pixel.connect("pixel_cleared", self, "_on_pixel_cleared")
		pixel.connect("pick_color", self, "_on_pick_color")

func _on_pick_color(pixel):
	$Container/Brush.color = pixel.color

func _on_pixel_set(pixel):
	pixel.color = $Container/Brush.color

func _on_pixel_cleared(pixel):
	pixel.color = Color(0,0,0,0)

func set_height(val:int)->void:
	height = val
	if !cleaning: _resize()

func set_width(val:int)->void:
	width = val
	if !cleaning: _resize()

func _on_Hgt_value_changed(value):
	set_height(value)

func _on_Wdt_value_changed(value):
	set_width(value)

func _on_Done_pressed():
	_on_Clean_pressed()
	yield(self, "done_cleaning")
	emit_signal("done")

func _on_Back_pressed():
	get_tree().change_scene("res://Scenes/Main.tscn")

func _on_Clean_pressed():
	if $Sprite.get_child_count() <= 1:
		return
	cleaning = true
	_clean()
	cleaning = false
	emit_signal("done_cleaning")

func _clean():
	var npixels = $Sprite.get_child_count()
	npixels = _clean_rows(npixels)
	npixels = _clean_cols(npixels)


func _clean_cols(npixels)->int:
	var cols := []
	
	var empty := true
	for x in width:
		var col := []
		for y in height:
			var pixel = $Sprite.get_child(x + y*width)
			if pixel.color.a != 0:
				empty = false
			col.append(pixel)
		if empty: cols.append(col)
		else: break
	
	empty = true
	for x in width:
		var col := []
		for y in height:
			var pixel = $Sprite.get_child((width-1-x) + y*width)
			if pixel.color.a != 0:
				empty = false
			col.append(pixel)
		if empty: cols.append(col)
		else: break
	
	for col in cols:
		for p in col:
			p.free()
			npixels-=1
	var ncols = ceil(npixels/height)
	$Sprite.columns = ncols
	$Wdt/Number.set_value(ncols)
	width = ncols
	return npixels

func _clean_rows(npixels)->int:
	var rows := []
	
	var empty := true
	for y in height:
		var row := []
		for x in width:
			var pixel = $Sprite.get_child(x + y*width)
			if pixel.color.a != 0:
				empty = false
			row.append(pixel)
		if empty: rows.append(row)
		else: break
	
	empty = true
	for y in height:
		var row := []
		for x in width:
			var pixel = $Sprite.get_child(x + (height-1-y)*width)
			if pixel.color.a != 0:
				empty = false
			row.append(pixel)
		if empty: rows.append(row)
		else: break
	
	for row in rows:
		for p in row:
			p.free()
			npixels-=1
	var nrows = ceil(npixels/width)
	$Hgt/Number.set_value(nrows)
	height = nrows
	return npixels

func _on_CheckBox_toggled(button_pressed):
	$SubName/Name.editable = button_pressed

func _on_Separate_pressed():
	clear_colors()
	var colors := {}
	for pix in $Sprite.get_children():
		if !colors.has(pix.color):
			colors[pix.color] = []
		colors[pix.color].append(pix)
	for col in colors.keys():
		create_color_group(col, colors[col])

func clear_colors():
	for child in $SeparateColors/PixelColors.get_children():
		child.queue_free()

func create_color_group(color:Color, pixelGroup:Array):
	if color.a == 0:
		return
	var group = COL.instance()
	group.color = color
	group.pixelGroup = pixelGroup
	$SeparateColors/PixelColors.add_child(group)

func _on_ClearDrawing_pressed():
	for child in $Sprite.get_children():
		child.queue_free()
		create_pixel()
	clear_colors()