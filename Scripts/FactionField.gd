extends HBoxContainer

const DEFAULT_REL = "NEUTRAL"

func _ready():
	$From.connect("relation_chosen", self, "_on_relation_chosen")
	$From.text = str("From ", self.name, ": ", Enum.Relation.keys()[Entities.factions[self.name]["default"]])
	$With.connect("relation_chosen", self, "_on_relation_chosen")
	$With.text = str("With ", self.name, ": ", DEFAULT_REL)

func _on_relation_chosen(relation, field_name):
	var field = get_node(field_name)
	field.text = str(field_name, " ", self.name, ": ", relation)
	if field_name == "From":
		get_parent().get_parent().fromRelations[self.name] = Enum.Relation[relation]
	else:
		get_parent().get_parent().toRelations[self.name] = Enum.Relation[relation]