extends Node

func _on_Start_pressed():
	get_tree().change_scene("res://Scenes/Game.tscn")

func _on_Editors_pressed():
	get_tree().change_scene("res://Tools/ToolsSelector.tscn")

func _on_Exit_pressed():
	get_tree().quit()
