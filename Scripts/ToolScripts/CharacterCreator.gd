tool
extends Node

export(Color) var top = Color(0,0,0,1) setget set_top
export(Color) var bottom = Color(1,1,1,1) setget set_bottom
export(Color) var hair_color = Color(0,0,0,1) setget set_hair_color

const BINPATH = "res://Assets/Sprites/.scount.bin"
const PATH = "res://Assets/Sprites/"
const EXT = ".png"

var hair = false setget set_hair
var tex
var img
var sprite

func _ready():
	top = $TopColor.color
	bottom = $BottomColor.color
	hair_color = $HairColor.color
	create_image()

func create_image(imgName:String="", close:bool=false):
	if sprite: sprite.queue_free()
	sprite = Sprite.new()
	img = Image.new()
	tex = ImageTexture.new()
	tex.create(3,6,Image.FORMAT_RGBA8, Texture.FLAG_MIPMAPS)
	img.create(3,6,true, Image.FORMAT_RGBA8)
	img.lock()
	for j in 6:
		for i in 3:
			if j < 3:
				img.set_pixel(i, j, top)
			else:
				img.set_pixel(i, j, bottom)
	if hair:
		for i in 3:
			img.set_pixel(i, 0, hair_color)
	img.set_pixel(1,5, Color(1,1,1,0))
	tex.set_data(img)
	sprite.set_centered(false)
	sprite.texture = tex
	sprite.scale = Vector2(2,2)
	sprite.position = Vector2(100,200)
	add_child(sprite)
	if imgName != "":
		var nName = str(PATH,imgName,EXT)
		if img.save_png(nName) == OK:
			print("Image saved as ", nName, " succesfully!")
			if close:
				get_tree().quit()

func set_top(n_color):
	top = n_color
	create_image()

func set_bottom(n_color):
	bottom = n_color
	create_image()

func set_hair(n_val):
	hair = n_val
	create_image()

func set_hair_color(n_color):
	hair_color = n_color
	create_image()

func _on_TopColor_color_changed(color):
	set_top(color)

func _on_BottomColor_color_changed(color):
	set_bottom(color)

func _on_HairColor_color_changed(color):
	set_hair_color(color)

func _on_CheckButton_toggled(button_pressed):
	$HairColor.set_visible(button_pressed)
	set_hair(button_pressed)

func _on_Done_pressed():
	create_image("char", true)

func _on_CreateAsset_pressed():
	var f = File.new()
	var n = 0
	if f.file_exists(BINPATH):
		f.open(BINPATH, File.READ)
		n = f.get_32()
		f.close()
		f = File.new()
	f.open(BINPATH, File.WRITE)
	f.store_32(n+1)
	create_image(str(n), false)