extends Node

export(int) var Variants

onready var obj = get_node("Object")
onready var data = Objects.Data

func _ready():
	reinitialize()

func reinitialize():
	if !data.keys().has(obj.objectName):
		data[obj.objectName] = create_entry()
	$VBox/Params.set_text(str(data[obj.objectName]))

func create_entry():
	var d = {}
	d["variant"] = {}
	d["vQtty"] = 0
	return d

func create_variant(dict:Dictionary, obj):
	var d = {}
	d["polygon"] = obj.get_node("Shapes/Occluder").occluder.polygon
	d["shapesPos"] = obj.get_node("Shapes").position
	dict["variant"][dict["vQtty"]] = d
	dict["vQtty"]+=1

func _on_Button_pressed():
	create_variant(data[obj.objectName], obj)
	reinitialize()

func _on_Done_pressed():
	Objects.store_data()
	get_tree().quit()

func _on_RM_pressed():
	data[obj.objectName] = create_entry()
	reinitialize()
