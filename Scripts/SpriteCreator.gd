extends HBoxContainer

const SPRITES_FOLDER = "res://Assets/Sprites/"
const RESULT = preload("res://Tools/Result.tscn")

var images := []

func create_results():
	var variations = $SpriteParams/ColorVariation/Variations/Number.value
	$Results/Images.columns = int(ceil(variations/sqrt(variations)))
	for res in $Results/Images.get_children():
		res.queue_free()
	images.clear()
	if $SpriteParams/ColorVariation/Slider.value == 0: variations = 1
	for i in variations:
		images.append(create_result(i))

func create_result(variant:int)->Image:
	var tex = RESULT.instance()
	var img = _create_image(variant>0)
	var texture = ImageTexture.new()
	texture.create_from_image(img)
	tex.texture = texture
	tex.rect_scale = Vector2(4,4)
	$Results/Images.add_child(tex)
	tex.connect("delete_result", self, "_delete_result")
	return img

func _delete_result(result):
	var idx = $Results/Images.get_children().find(result)
	result.queue_free()
	images.remove(idx)

func _create_image(vary:bool = false)->StreamTexture:

	var img = Image.new()
	var w = $SpriteParams.width
	var h = $SpriteParams.height
	var colors := []
	var pixels := {}
	
	img.create(w,h,true, Image.FORMAT_RGBA8)
	img.lock()
	
	var color_found = false
	for y in h:
		for x in w:
			var col = $SpriteParams/Sprite.get_child(x + y*w).color
			if !colors.has(col):
				colors.append(col)
			pixels[Vector2(x,y)] = colors.find(col)
	if vary:
		for color in colors:
			var ocol = color
			var mult = $SpriteParams/ColorVariation/Slider.value
			color.r *= 1 - randf()*mult
			color.g *= 1 - randf()*mult
			color.b *= 1 - randf()*mult
			colors[colors.find(ocol)] = color
	for pix in pixels.keys():
		img.set_pixelv(pix, colors[pixels[pix]])
	return img

func _on_SpriteParams_done():
	create_results()

func _on_Save_pressed():
	if $SpriteParams/Name.text == "SpriteName":
		print("No name given, nothing saved.")
		return
	for image in images:
		var folderName = $SpriteParams/Name.text
		var subfolderName := ""
		if $SpriteParams/SubName/CheckBox.pressed:
			subfolderName = $SpriteParams/SubName/Name.text
		create_dir(folderName, subfolderName)
		var path = ""
		if subfolderName == "":
			path = str(SPRITES_FOLDER, folderName, "/", images.find(image),".png")
		else:
			path = str(SPRITES_FOLDER, folderName, "/", subfolderName,"/", images.find(image),".png")
		var d = Directory.new()
		if image.save_png(path) == OK:
			print("Image saved at: ", path)
		else:
			print("Error saving image.")

func create_dir(folderName:String, subfolder:String):
	var d = Directory.new()
	var dir = ""
	if subfolder != "":
		dir = str(SPRITES_FOLDER,"/",folderName,"/",subfolder)
	else:
		dir = str(SPRITES_FOLDER,"/",folderName)
	if !d.dir_exists(folderName):
		if d.make_dir_recursive(dir) != OK:
			print("Error creating folder.")
		else:
			print("Folder created succesfully!")
