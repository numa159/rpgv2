extends Node2D
class_name Bullet

const LIFETIME = 5

export(float) var lifeTime = 5 setget set_lifeTime, get_lifeTime
export(int) var speed = 2
export(int) var damage = 10

var attackType = Enum.AttackTypes.RANGED

var emitter = null
var direction :Vector2
var fired := false
var frame:float = 0
var lifeTimer := Timer.new()
var charge := .0

signal fired

func _ready():
	set_physics_process(false)
	connect("fired", self, "_on_fire")
	add_child(lifeTimer)
	lifeTimer.connect("timeout", self, "_on_timeout")
	lifeTimer.set_wait_time(lifeTime)

func set_lifeTime(nval):
	lifeTime = nval
	lifeTimer.set_wait_time(nval)

func get_lifeTime()->float:
	return lifeTime

func _on_timeout():
	collide()

func _on_fire():
	if !direction:
		print("ERROR: No direction given")
		collide()
	rotation = direction.angle()
	fired = true
	set_physics_process(true)
	lifeTimer.start()

func _physics_process(delta):
	frame+=delta
	fly(direction, frame)

func fly(dir, delta):
	position += Vector2(speed, 0).rotated(dir.angle())

func _on_Area_body_entered(body):
	if !fired:
		return
	if body is KinematicBody2D and body != emitter:
		hit(body)
	elif body != emitter:
		collide()

func collide():
	lifeTimer.stop()
	fired = false
	$Sprite.hide()
	$Effect.set_emitting(false)
	$Effect.hide()
	$CollisionEffect.set_emitting(true)
	$Area.set_monitoring(false)
	yield(get_tree().create_timer($CollisionEffect.lifetime/$CollisionEffect.speed_scale), "timeout")
	self.call_deferred("free")

func hit(body):
	body.recieve_damage(damage, emitter)
	collide()
