extends ChunkPopulator

const NOISE_MULT = 2
const OBJ_SEP_MULT = 3.0

const MIN_SEP = 2.0
const MAX_SEP = 10.0

enum TILES{Grass, Bricks}

var noise = OpenSimplexNoise.new()
var obj_spawned = 0

signal chunk_created

var ruins : bool = true

func set_seed(n_seed):
	if n_seed:
		_seed = n_seed
		seed(_seed.hash())
		noise.set_seed(_seed.hash())

func populate_chunk(chunk):
	if _seed == null:
		print("ERROR: no seed provided")
		return
	populate_floor(chunk)
	populate_objects(chunk)
	emit_signal("chunk_created", chunk)

func unload_chunk(chunk):
	if !chunk.unloaded:
		for entity in GameData.EntitiesSpace.get_children():
			var eChunkPos = (entity.global_position / chunk.cellSize) / chunk.size
			eChunkPos = Vector2(floor(eChunkPos.x), floor(eChunkPos.y))
			if eChunkPos == chunk.idx:
				var entity_data = {}
				entity_data["hp"] = entity.health
				entity_data["pos"] = entity.global_position
				entity_data["ref"] = entity.ref
				entity_data["sidx"] = entity.spriteIdx
				chunk.entities.append(entity_data)
				entity.queue_free()
		var i = 0
		for object in chunk.objects:
			if object["type"] == "CampFire":
				chunk.objects[i]["state"] = object.ref.state
			object.ref.queue_free()
			i+=1

func load_chunk(chunk):
	if chunk.unloaded:
		for entity in chunk.entities:
			var n_entity = Entities.EntRef[entity["ref"]].instance()
			n_entity.global_position = entity["pos"] + Vector2(50*randf(),0).rotated(randf()*2*PI)
			n_entity.health = entity["hp"]
			n_entity.spriteIdx = entity["sidx"]
			GameData.EntitiesSpace.add_child(n_entity)
		chunk.entities = []
		for object in chunk.objects:
			var obj = Objects.new_object(object.type, object.var)
			obj.position = object.pos
			object.ref = obj
			if obj is SpawnerObject:
				obj.hasSpawned = true
				if obj.objectName == "CampFire":
					obj.state = object["state"]
			Generator.spaces.get_node("Objects").add_child(obj)

func populate_floor(chunk):
	for x in chunk.size:
		for y in chunk.size:
			var cell_pos = chunk.pos + Vector2(x, y)
			var value = 0
			if ruins:
				value = round(abs(noise.get_noise_2d(float(cell_pos.x*NOISE_MULT), float(cell_pos.y*NOISE_MULT))))
			chunk.cells[x][y] = {"value":value}
			Generator.spaces.get_node("Floor").set_cellv(cell_pos, value)
	Generator.spaces.get_node("Floor").update_bitmask_region(chunk.pos, chunk.pos + Vector2(chunk.size-1,chunk.size-1))

func populate_objects(chunk):
	var size_to_populate = Vector2(chunk.size * chunk.cellSize, chunk.size * chunk.cellSize)
	var div = noise.get_noise_2dv(chunk.pos)
#	var div = sin(chunk.pos.length()/200.0) + .00001
	div = 1-abs(div)
#	div *= div
	div += .00001
	
	var separation = chunk.cellSize * OBJ_SEP_MULT  / div
	if separation < 45.0:
		print("limit reached")
		separation = 45.0
	elif separation > 500.0:
		separation = 500.0
	var points = PoissonDiscSampling.generate_points(separation, size_to_populate)
	if points.size() == 0:
		return
	for point in points:
		var global_point = point+chunk.pos*chunk.cellSize
		var cell_pos = Generator.spaces.get_node("Floor").world_to_map(global_point)
		if chunk.cells[cell_pos.x-chunk.pos.x-1][cell_pos.y-chunk.pos.y-1].value == 0:
			var v = obj_spawned % 200
			#var v2 = (obj_spawned*obj_spawned) % 200
			if v == 100:
				spawn_obj(global_point, chunk, "CampFire")
			elif v == 0:
				spawn_obj(global_point, chunk, "RockBig")
			elif v < 3:
				spawn_obj(global_point, chunk, "RockMedium")
			elif v < 8:
				spawn_obj(global_point, chunk, "RockSmall")
			else:
				spawn_obj(global_point, chunk, "TreeMedium")

func spawn_obj(pos, chunk, oname):
	var obj = Objects.new_object(oname, pos.x+pos.y)
	obj.position = pos
	Generator.spaces.get_node("Objects").add_child(obj)
	obj_spawned+=1
	chunk.objects.append({"type":oname,"pos":pos,"ref":obj,"var":obj.type})