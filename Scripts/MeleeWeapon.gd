extends Node2D
class_name MeleeWeapon

export(float) var attackSpeed = 1
export(float) var attackCD = 1
export(float) var damage = 1

onready var trail = get_node("Sprite/Area2D/CollisionShape2D/Trail")
onready var rotationTween = get_node("rotationTween")

var attackType = Enum.AttackTypes.MELEE

var heldBy = null
var wpnRange := 0.0

var _hurtEntities := []

func _ready():
	wpnRange = abs(($Sprite/Area2D/CollisionShape2D.global_position - global_position).y) + $Sprite/Area2D/CollisionShape2D.shape.extents.y

func attack(direction:Vector2, spd : float):
	if !heldBy:
		print("Error, weapon not held")
		return
	show()
	$Sprite/Area2D.set_monitoring(true)
	heldBy.attacking = true
	var rot = self.rotation
	var angle = direction.angle() + (PI/2.0)
	var dir = 0
	if angle > rotation:dir = 1
	else:dir = -1
	self.scale.x = -dir
	trail.texture = $Sprite.texture
	trail.emitting = true
	modulate = Color.white
	
	rotationTween.start()
	rotationTween.interpolate_property(self, "rotation", self.rotation, angle + dir*(PI/2.0), 1/(attackSpeed*spd), Tween.TRANS_LINEAR, Tween.EASE_OUT)
	yield(rotationTween, "tween_completed")
	
	trail.emitting = false
	$Sprite/Area2D.set_monitoring(false)
	_hurtEntities.clear()
	
	rotationTween.interpolate_property(self, "modulate", self.modulate, Color(1,1,1,0), attackCD/spd, Tween.TRANS_BACK,Tween.EASE_IN)
	yield(rotationTween, "tween_completed")
#	yield(get_tree().create_timer(attackCD/spd), "timeout")
	heldBy.attacking = false
	hide()

func hurt(body):
	if body != heldBy and heldBy.attacking and !body is StaticBody2D and !_hurtEntities.has(body):
		body.recieve_damage(damage, heldBy)
		_hurtEntities.append(body)
