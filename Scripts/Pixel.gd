extends ColorRect

signal pixel_set
signal pixel_cleared
signal pick_color

func _on_Pixel_gui_input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		emit_signal("pixel_set", self)
	elif event is InputEventMouseButton and event.button_index == BUTTON_RIGHT:
		emit_signal("pixel_cleared", self)
	elif event is InputEventMouseButton and event.button_index == BUTTON_MIDDLE:
		emit_signal("pick_color", self)
