extends Bullet

export(float) var maxSpeed = 1.0

func fly(dir, delta):
	if charge < .75 :
		collide()
	var ch = charge*charge
	position += Vector2(speed*min(ch, maxSpeed), 0).rotated(dir.angle())
