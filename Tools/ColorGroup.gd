extends ColorPickerButton

var pixelGroup := []

func _ready():
	if pixelGroup.empty():
		print("Error, no pixels given.")
		assert(false)

func _on_Color_gui_input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_MIDDLE:
		emit_signal("pick_color", self)

func _on_Color_color_changed(newColor):
	for pix in pixelGroup:
		pix.color = newColor