extends TextureRect

signal delete_result

func _on_Result_gui_input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_RIGHT:
		emit_signal("delete_result", self)
