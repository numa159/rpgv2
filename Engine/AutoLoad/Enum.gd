extends Node

enum AttackTypes {RANGED, MELEE}
enum DamageTypes {BLUNT, PIERCING, SLASHING, FIRE}
enum Relation {NEUTRAL, HOSTILE, FLEE, ALLY}