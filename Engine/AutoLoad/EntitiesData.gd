extends Node

const PATH = "res://Engine/DB/Entities.json"

const AttackScenes = {
		"Greatsword":preload("res://Scenes/Entities/AttackScenes/Greatsword.tscn"),\
		"Sword":preload("res://Scenes/Entities/AttackScenes/Sword.tscn"),\
		"Axe":preload("res://Scenes/Entities/AttackScenes/Axe.tscn"),\
		}
const EntRef = {\
		"Humanoid" : preload("res://Scenes/Entities/Primitive/Humanoid.tscn"),\
		}

var factions = {}
var subFactions = {}

var Data = {"factions" : {}, "subFactions" : {}}

func _ready():
	load_data()
	factions = Data["factions"]
	subFactions = Data["subFactions"]

func _exit_tree():
	Data["factions"] = factions
	Data["subFactions"] = subFactions
	store_data()

func store_data():
	var f = File.new()
	f.open(PATH, File.WRITE)
	f.store_line(to_json(Data))
	f.close()

func load_data():
	var f = File.new()
	if !f.file_exists(PATH):
		return
	f.open(PATH, File.READ)
	var data = {}
	data = parse_json(f.get_as_text())
	Data = data