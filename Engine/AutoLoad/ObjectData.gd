extends Node

const PATH = "res://Engine/DB/Objects.json"

const Refs = {\
	"RockBig":preload("res://Scenes/Populators/Objects/RockBig.tscn"),\
	"TreeMedium":preload("res://Scenes/Populators/Objects/TreeMedium.tscn"),\
	"RockMedium":preload("res://Scenes/Populators/Objects/RockMedium.tscn"),\
	"RockSmall":preload("res://Scenes/Populators/Objects/RockSmall.tscn"),\
	"CampFire":preload("res://Scenes/Populators/Objects/CampFire.tscn"),\
	}

var Data = {}
# DATA CONTENTS BACKUP
#	"RockBig":{"vQtty":5,"variant":{0:{"polygon":[Vector2(-17.4, 0),Vector2(23.4, 0),Vector2(17, 8),Vector2(2, 10),Vector2(-11, 7)],"shapesPos":Vector2(-1.5, 0)},1:{"polygon":[Vector2(-17.4, 0),Vector2(23.4, 0),Vector2(17, 8),Vector2(2, 10),Vector2(-11, 7)],"shapesPos":Vector2(2.2, -0.3)},2:{"polygon":[Vector2(-15, 0),Vector2(21, 0),Vector2(15, 7),Vector2(2, 9),Vector2(-10, 6)],"shapesPos":Vector2(2.2, -0.3)},3:{"polygon":[Vector2(-22, 0),Vector2(17, 0),Vector2(19, 7),Vector2(7, 10),Vector2(-10, 9)],"shapesPos":Vector2(2.2, -0.3)},4:{"polygon":[Vector2(-21, 0),Vector2(19, 0),Vector2(16, 7),Vector2(3, 10),Vector2(-11, 8)],"shapesPos":Vector2(2.2, -0.3)}}},"TreeMedium":{"vQtty":6,"variant":{0:{"polygon":[Vector2(-4.5, 0),Vector2(7.2, 0),Vector2(1.35, 4.05)],"shapesPos":Vector2(0, 0)},1:{"polygon":[Vector2(-4.5, 0),Vector2(7.2, 0),Vector2(1.35, 4.05)],"shapesPos":Vector2(0, 0)},2:{"polygon":[Vector2(-4.5, 0),Vector2(7.2, 0),Vector2(1.35, 4.05)],"shapesPos":Vector2(-2, 0)},3:{"polygon":[Vector2(-4.5, 0),Vector2(7.2, 0),Vector2(1.35, 4.05)],"shapesPos":Vector2(-2, 0)},4:{"polygon":[Vector2(-4.5, 0),Vector2(7.2, 0),Vector2(1.35, 4.05)],"shapesPos":Vector2(-2.5, 0)},5:{"polygon":[Vector2(-4.5, 0),Vector2(7.2, 0),Vector2(1.35, 4.05)],"shapesPos":Vector2(-5, 0)}}}

func _ready():
	load_data()

func _exit_tree():
	store_data()

func store_data():
	var f = File.new()
	f.open(PATH, File.WRITE)
	f.store_line(to_json(Data))
	f.close()

func load_data():
	var f = File.new()
	if !f.file_exists(PATH):
		return
	f.open(PATH, File.READ)
	var data = {}
	data = parse_json(f.get_as_text())
	var processed_data = {}
	for key in data.keys():
		var d = {}
		for skey in data[key].keys():
			if skey == "vQtty":
				d[skey] = data[key][skey]
			elif skey == "variant":
				var p = {}
				for sskey in data[key][skey].keys():
					var q = {}
					for ssskey in data[key][skey][sskey]:
						if ssskey == "shapesPos":
							var arr = data[key][skey][sskey][ssskey].trim_prefix("(").trim_suffix(")").split_floats(", ")
							q[ssskey] = Vector2(arr[0], arr[1])
						elif ssskey == "polygon":
							var pol:PoolVector2Array = []
							var string = data[key][skey][sskey][ssskey]
							var arr = string.trim_prefix("[(").trim_suffix(")]").split("), (")
							for val in arr:
								var narr = val.split_floats(", ")
								pol.append(Vector2(narr[0], narr[1]))
							q[ssskey] = pol
					p[int(sskey)] = q
				d[skey] = p
		processed_data[key] = d
	Data = processed_data

func new_object(oName:String, vType:int):
	var type = vType * vType
	var instance = Refs[oName].instance()
	instance.initialize(oName, type % int(Data[oName].vQtty))
	return instance
