tool
extends Node
class_name PoissonDiscSampling

const SQR_TWO = 1.4142136
const DEFAULT_MAX = 30

static func generate_points(radius:float, sampleRegionSize:Vector2, maxSamples:int = DEFAULT_MAX) -> PoolVector2Array:
	var cell_size:float =  radius / sqrt(2.0)
	
	var grid = []
	for i in ceil(sampleRegionSize.x/cell_size):
		var arr : PoolIntArray = []
		for i in ceil(sampleRegionSize.y/cell_size):
			arr.append(0)
		grid.append(arr)
	
	var points:Array = []
	var spawnPoints:Array = []
	var candidate_accepted = false
	
	spawnPoints.append(sampleRegionSize/2.0)
	while(spawnPoints.size() != 0):
		candidate_accepted = false
		var spawnIndex = 0
		var spawnCentre : Vector2 = spawnPoints[spawnIndex]
		for i in maxSamples:
			var angle : float = randf() * PI * 2.0
			var dir = Vector2(sin(angle), cos(angle))
			var candidate = spawnCentre + dir * rand_range(radius, radius * 2.0)
			if !points.has(candidate):
				if is_valid(candidate, sampleRegionSize, cell_size, radius, points, grid):
					points.append(candidate)
					spawnPoints.append(candidate)
					grid[floor(candidate.x/cell_size)][floor(candidate.y/cell_size)] = points.size()
					candidate_accepted = true
		if !candidate_accepted:
			spawnPoints.remove(spawnIndex)
	return PoolVector2Array(points)

static func is_valid(candidate:Vector2, size:Vector2, cell_size:float, radius:float, points:Array, grid:Array)->bool:
	
	if candidate.x >= 0 and candidate.x < size.x and candidate.y >= 0 and candidate.y < size.y:
		var cellX = int(candidate.x/cell_size)
		var cellY = int(candidate.y/cell_size)
		var searchStartX = max(0, cellX-2)
		var searchEndX = min(cellX+2, grid.size())
		var searchStartY = max(0, cellY-2)
		var searchEndY = min(cellY+2, grid[0].size())
		
		for x in range(searchStartX, searchEndX):
			for y in range(searchStartY, searchEndY):
				if grid[x][y]:
					var pointIndex = grid[x][y] - 1
					if pointIndex != -1:
						var sqrDst:float = (candidate - points[pointIndex]).length_squared()
						if (sqrDst < radius*radius):
							return false
		return true
	return false