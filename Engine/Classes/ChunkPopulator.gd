extends Node
class_name ChunkPopulator

var Generator = null
var _seed setget set_seed, get_seed

func populate_chunk(chunk):
	pass

func clear_chunk(chunk):
	pass

func set_seed(n_seed):
	_seed = n_seed
	seed(_seed.hash())

func get_seed() -> String:
	return _seed