extends WorldObject
class_name SpawnerObject

const MIN_DIST = 10

export(int) var spawnLimit = 0
export(int) var spawnRange = 0
export(PackedScene) var spawnableEntity = null

var hasSpawned := false

func _ready():
	_spawn_entities()

func _spawn_entities():
	if hasSpawned:
		return
	if spawnRange == 0 || spawnableEntity == null || spawnLimit == 0:
		print("Error, invalid spawner")
		return
	var spawnPoints = PoissonDiscSampling.generate_points(MIN_DIST, Vector2.ONE * spawnRange)
	var i = 0
	var maxSpawns = randi() % (spawnLimit-1) +1
	for point in spawnPoints:
		if i == maxSpawns:
			break
		var sPos = point - Vector2.ONE * spawnRange/2.0 + self.position
		var enem = spawnableEntity.instance()
		enem.position = sPos
		GameData.EntitiesSpace.add_child(enem)
		i+=1
