tool
extends Node2D
class_name WorldObject

const OCC_MULT = .99
const NAV_MULT = 1.2

export(int,0,99) var type = 0 setget set_type
export(String) var objectName = ""

var navPoly:NavigationPolygon

func initialize(oName:String, vType:int):
	if !Objects.Data.has(oName):
		print("Invalid Object")
		self.queue_free()
		return
	set_type(vType)

func set_type(n_type):
	type = n_type
	$Foreground.set_frame(type)
	$Background.set_frame(type)
	upd_shapes(type)

func upd_shapes(type):
	var arr = Objects.Data[objectName].variant[type].polygon
	var pos = Objects.Data[objectName].variant[type].shapesPos
	set_polygon(arr, pos, arr.size())

func set_polygon(array:PoolVector2Array, pos:Vector2, size):
	var col : PoolVector2Array = []
	var occ : PoolVector2Array = []
	var pol : PoolVector2Array = []
	for i in size:
		col.insert(i, array[i])
		occ.insert(i, array[i]*OCC_MULT)
		pol.insert(i, array[i]*NAV_MULT)
	var npol = NavigationPolygon.new()
	npol.add_outline(pol)
	npol.make_polygons_from_outlines()
	
	navPoly = npol
	get_node("Shapes").position = pos
	get_node("Shapes").get_node("Collision/Shape").shape.points = col
	get_node("Shapes").get_node("Occluder").occluder.polygon = occ