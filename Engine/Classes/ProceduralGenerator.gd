extends Node
class_name ProceduralGenerator

const CELL_SIZE = 16
const CHUNK_SIZE = 10
const RANGE = 3
const INIT_RANGE = 3

export(String) var _seed = null setget set_seed, get_seed

var d := 0

var chunkLocale := Vector2.ZERO
var loadThread := Thread.new()
var loadMutex := Mutex.new()
var spaces = null
var populators = null
var loadedChunks = {}
var createdChunks = {}
var chunkPopulator = null
var characterLocal : Vector2

func set_seed(n_seed):
	_seed = n_seed

func get_seed() -> String:
	return _seed

func _ready():
	initialize()

func _process(delta):
	var n_locale = (CharacterData.pos / CHUNK_SIZE) / CELL_SIZE
	n_locale = Vector2(floor(n_locale.x), floor(n_locale.y))
	if n_locale != chunkLocale:
		check_in_range([RANGE, null])
		chunkLocale = n_locale

func initialize():
	if _seed == null:
		set_seed(String(OS.get_system_time_secs()))
	spaces = get_node("Spaces")
	populators = get_node("Populators")
	for populator in populators.get_children():
		populator.Generator = self
		populator.set_seed(_seed)
	chunkPopulator = populators.get_node("WoodsPopulator")
	chunkPopulator.ruins = false
	check_in_range([INIT_RANGE, null])

func check_in_range(args:Array)->void:
	var ran = args[0]
	var thread = args[1]
	var local = CharacterData.pos
	var chunks_checked = []
	for x in range(-ran,ran+1):
		for y in range(-ran,ran+1):
			var offs = Vector2(x,y)
			var chunk_to_check = (local / CHUNK_SIZE) / CELL_SIZE + offs
			chunk_to_check = Vector2(floor(chunk_to_check.x), floor(chunk_to_check.y))
			check_chunk(chunk_to_check)
			chunks_checked.append(chunk_to_check)
	if !thread:
		for chunk_idx in loadedChunks.keys():
			if !chunks_checked.has(chunk_idx):
				loadedChunks[chunk_idx].unload()
				loadedChunks.erase(chunk_idx)
		print($Spaces/Objects.get_children().size())
	else:
		_load_finished(thread, chunks_checked)

func _load_finished(thread:Thread, chunks_checked):
	if thread.is_active(): thread.wait_to_finish()
	for chunk_idx in loadedChunks.keys():
		if !chunks_checked.has(chunk_idx):
			loadedChunks[chunk_idx].unload()
			loadedChunks.erase(chunk_idx)
	print("finished ", d)
	d+=1
	print($Spaces/Objects.get_children().size())

func check_chunk(location:Vector2):
	var grid_pos = location
	if createdChunks.has(grid_pos):
		if !loadedChunks.has(grid_pos):
			var chunk = createdChunks[grid_pos]
			chunk.reload()
			loadedChunks[chunk.idx] = chunk
		return
	create_chunk(grid_pos, chunkPopulator)

func create_chunk(chunk_pos, populator):
	var chunk = Chunk.new(chunk_pos, populator, CHUNK_SIZE, CELL_SIZE)
	chunk.create()
	loadedChunks[chunk.idx] = chunk
	createdChunks[chunk.idx] = chunk

class Chunk:
	var unloaded : bool = true
	var created : bool = false
	var pos : Vector2
	var idx : Vector2
	var populator = null
	var size = 0
	var cellSize = 0
	var cells = []
	var objects = []
	var entities = []
	
	func _init(chunkPos, chunkPopulator, chunkSize, chunkCellSize):
		idx = chunkPos
		size = chunkSize
		pos = idx * size
		populator = chunkPopulator
		cellSize = chunkCellSize
		for i in size:
			var col = []
			col.resize(size)
			cells.append(col)
	
	func create():
		if !created:
			populator.populate_chunk(self)
			created = true
			unloaded = false
	
	func unload():
		if created:
			populator.unload_chunk(self)
			unloaded = true
	
	func reload():
		if unloaded:
			populator.load_chunk(self)
			unloaded = false
	
	func _exit_tree():
		populator.clear_chunk(self)
